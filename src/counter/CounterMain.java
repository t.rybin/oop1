package counter;

import counter.Counter;

public class CounterMain {
    public static void main(String[] args) {
        Counter counter1 = new Counter(1, 14, 13);
        display(counter1);
    }
    public static void display(Counter counter1){
        System.out.println(counter1.getCurrent());
        counter1.dec();
        System.out.println("decrease " + counter1.getCurrent());
        counter1.inc();
        System.out.println("increase " + counter1.getCurrent());
        counter1.inc();
        System.out.println("increase " + counter1.getCurrent());
        counter1.inc();
        System.out.println("increase " + counter1.getCurrent());
    }
}

package test2;

class Test2 {
    private int x;
    private int y;
    public Test2(int x, int y) {
       this.x = x;
       this.y = y  ;
    }
    public int getX() {
        return x;
    }
    public int getY() { return y; }
    public void setX(int a) {
        x = a;
    }
    public void setY(int b) {
        y = b;
    }
}

package clock;

import java.util.Scanner;
class Time {
    private int hour, minutes, sec;
    public Time(int hour, int minutes, int sec) {
        setHour(hour);
        setMinutes(minutes);
        setSec(sec);
    }
    public Time() {
        this(0, 0, 0);
    }
    public void setHour(int hour) {
        if (hour < 0 || hour > 23)
            this.hour = 0;
        else
            this.hour = hour;
    }
    public void setMinutes(int minutes) {
        if (minutes < 0 || minutes > 59)
            this.minutes = 0;
        else
            this.minutes = minutes;
    }
    public void setSec(int sec) {
        if (sec < 0 || sec > 59)
            this.sec = 0;
        else
            this.sec = sec;
    }
    public int getHour() {
        return hour;
    }
    public int getMinutes() { return minutes; }
    public int getSeconds() {
        return sec;
    }
    public String toString() {
        return String.format("%02d:%02d:%02d", this.hour, this.minutes, this.sec);
    }

    public Time read() {
        Time time = new Time();
        Scanner scanner = new Scanner(System.in);
        System.out.println("hour: ");
        setHour(scanner.nextInt());

        System.out.println("min: ");
        setMinutes(scanner.nextInt());

        System.out.println("sec: ");
        setSec(scanner.nextInt());

        return time;
    }
    public void addSeconds(int sec) {
        this.sec += sec;
        this.minutes += this.sec / 60;
        this.sec = this.sec % 60;
        this.hour += this.minutes / 60;
        this.minutes = this.minutes % 60;
        this.hour = this.hour % 24;
    }
    public void addmMinutes(int hour) {
        addSeconds(minutes * 60);
    }
    public void addHours(int hour) {
        addSeconds(hour * 60 * 60);
    }
}